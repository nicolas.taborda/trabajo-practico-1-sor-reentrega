#include "pthread.h"

#include "stdio.h"

#include "stdlib.h"

#define CantidadDeDiviciones 8 //solo puede ser numero pares o 1 

    int numberArray[10000];
    int sumadortotal;
    void *funcion (void *inicio )
    {

       int posiciondeinicio = (int)inicio; // parametro de inicio para recorrer el arreglo con el for
       int posiciondefinalizacion = posiciondeinicio+(((sizeof(numberArray))/sizeof(int))/CantidadDeDiviciones);
       //parametro de finalizacion para recorrer el arreglo con el for

       int sumador=0;
       for (int i=posiciondeinicio; i< posiciondefinalizacion; i++)
       {
        sumador+=numberArray[i];
       } 
       sumadortotal+=sumador;
       //printf("suma parcial%d\n",sumador);
    }

    int main(void)
    {
      FILE *myFile;
      myFile = fopen("10milDigitosDePi_separados.txt", "r");

      //leer archivo en un array
    
      int i;
      if (myFile == NULL){
        printf("Error en la lectura\n");
        exit (0);
      }
      for (i = 0; i < 10000; i++){
        fscanf(myFile, "%d,", &numberArray[i] );
      }
      fclose(myFile);
      pthread_t threads[CantidadDeDiviciones];
      // genera un array de largo de la cantidad definida CantidadDeDiviciones que contine threads.
      for (int x = 0; x < CantidadDeDiviciones; x++)
      {
          //recorre la CantidadDeDiviciones
          int num=x; 
          int PosicionDeInicio= (x*(((sizeof(numberArray))/sizeof(int))/CantidadDeDiviciones));
          //calcula la pociosion inicial donde empieza la lectura dentro del arreglo de numero pi  
          pthread_create(&threads[x],NULL,funcion,(void *)PosicionDeInicio );
          // se crea un threads y se a como parametro la posicion de inicio de lectura del threads dentro del arreglo del numero pi
      }
      for (int i = 0; i < CantidadDeDiviciones; i++) 
      {
          pthread_join(threads[i], NULL);
          // ejecuta los thread 
        
      }
      printf("sumandor total %d",sumadortotal);
      pthread_exit(NULL);
      return 0;
    }
