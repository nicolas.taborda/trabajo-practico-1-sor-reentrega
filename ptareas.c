#include <pthread.h>
#include "stdio.h"
#include "stdlib.h"

int digitosPi[10000];


void * promedio(void *arg)
{
    int suma = 0;
    int cantidadElementos = 0;
    for (int i = 0; i < 10000; i++)
    {
        cantidadElementos++;
        suma = suma + digitosPi[i];
    }
    double totalPromedio = suma / cantidadElementos;
    printf("EL promedio es: %f \n", totalPromedio);
    return((void *)0);
}

void * digitos (void * arg)
{
    int d0= 0;
    int d1= 0;
    int d2= 0;
    int d3= 0;
    int d4= 0;
    int d5= 0;
    int d6= 0;
    int d7= 0;
    int d8= 0;
    int d9 = 0;
    int digitos[] = {d0, d1, d2, d3, d4, d5, d6, d7, d8, d9};
    int mayor = 0 ; 

     for (int i = 0; i < 10000; i++)
    {
      switch (digitosPi[i])
        {
        case 0:
            d0++;
        case 1:
            d1++;
        case 2:
            d2++;
        case 3:
            d3++;
        case 4:
            d4++;
        case 5:
            d5++;
        case 6:
            d6++;
        case 7:
            d7++;
        case 8:
            d8++;
        case 9:
            d9++;
        }
    }
    
    for (int i = 0; i < 10; i++)
    {
        if( digitos[i] > mayor);
            mayor = i;
    }

    printf("El digito que mas se repite es: %i \n",mayor);
}

void * primos (void *arg)
{
    long cantidadPrimos = 0;
    long sumador = 0;

    for (int i = 0; i < 10000 ; i++)
    {
        int divisores = 0 ;
        for (int j = 1; j <= digitosPi[i]; j++)
        {
            if ( digitosPi[i] % j == 0)
            {   
                divisores++;
            }
            if (divisores == 2 || digitosPi[i] == 1)
            {  
                cantidadPrimos++;
                sumador = sumador + digitosPi[i];
            } 
        }  
    }
    printf("La cantidad total de primos es: %li , y la suma de todos ellos da: %li \n", cantidadPrimos, sumador);
}


int main(void)
{
    pthread_t t_promedio;
    pthread_t t_digitos; 
    pthread_t t_primos;
    
    // creacion del arreglo pi y lectura del archivo
    
    FILE *archivo;
    archivo = fopen("10milDigitosDePi_separados.txt", "r");

    if (archivo == NULL)
    {
        printf("Error en la lectura\n");
        exit(0);
    }

    for (int i = 0; i < 10000; i++)
    {
        fscanf(archivo, "%i,", &digitosPi[i]);
    }

    fclose(archivo);

    pthread_create(&t_promedio, NULL, promedio , NULL);
    pthread_create(&t_digitos,NULL, digitos, NULL);
    pthread_create(&t_primos,NULL, primos, NULL);

    pthread_join(t_promedio,NULL);
    pthread_join(t_digitos,NULL);
    pthread_join(t_primos,NULL);

    pthread_exit(NULL);

    return 0;
}