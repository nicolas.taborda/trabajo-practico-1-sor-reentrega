#include <stdio.h>
#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;

#------------------------------------------------------
# TODO: Completar con su path
#------------------------------------------------------
proyectoActual="https://gitlab.com/nicolas.taborda/trabajo-practico-1-sor-.git"

#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
	
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $proyectoActual";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t f.  Abrir en terminal";        
    echo -e "\t\t\t g.  Abrir en carpeta";
    echo -e "\t\t\t h.  Info de proceso";
    echo -e "\t\t\t i.  Bomba fork";
    echo -e "\t\t\t j.  Shell";
    echo -e "\t\t\t k.  Explicacion output";
    echo -e "\t\t\t l.  Demo mutex ";
    echo -e "\t\t\t m.  Demo sincronizacion";
    echo -e "\t\t\t n.  Demo paralelismo";
    
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
    	imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
	echo "---------------------------"        
	echo "Somthing to commit?"
        decidir "cd $proyectoActual; git status";

        echo "---------------------------"        
	echo "Incoming changes (need a pull)?"
	decidir "cd $proyectoActual; git fetch origin"
	decidir "cd $proyectoActual; git log HEAD..origin/master --oneline"
}

b_funcion () {
       	imprimir_encabezado "\tOpción b.  Guardar cambios";
       	decidir "cd $proyectoActual; git add -A";
       	echo "Ingrese mensaje para el commit:";
       	read mensaje;
       	decidir "cd $proyectoActual; git commit -m \"$mensaje\"";
       	decidir "cd $proyectoActual; git push";
}

c_funcion () {
      	imprimir_encabezado "\tOpción c.  Actualizar repo";
      	decidir "cd $proyectoActual; git pull";   	 
}


f_funcion () {
	imprimir_encabezado "\tOpción f.  Abrir en terminal";        
	decidir "cd $proyectoActual; xterm &";
}

g_funcion () {
	imprimir_encabezado "\tOpción g.  Abrir en carpeta";        
	decidir "gnome-open $proyectoActual &";
}

#------------------------------------------------------
# TODO: Completar con el resto de ejercicios del TP, una funcion por cada item
#------------------------------------------------------

#funcion info de procesos 

h_funcion ( ) {
echo "Ingrese PID del proceso:";
read pid;
grep Name /proc/${pid}/status; 
grep State /proc/${pid}/status;
grep '^Pid' /proc/${pid}/status;
grep '^PPid' /proc/${pid}/status;
grep ctxt_switches /proc/${pid}/status;
}

i_funcion()
{
    echo "3....2...1.... BUM";
    gcc -o bombaFork bombaFork.c
    ./bombaFork
}

j_funcion()
{
    echo "Ingresando al interprete de shell";
    gcc -o shell shell2.c
    ./shell
}

k_funcion()
{
    echo " ";
    echo "Justificar el output del siguiente programa: ";
    echo " ";
    echo "#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
int main()
{
    fork();  
    printf("Hola mundo!\n");
    return 0;
}";
echo " ";
    echo "Respuesta: ";
    echo " ";
    echo "Cuando un proceso genera un proceso hijo mediante fork(), luego de que el proceso hijo
es creado ambos procesos van a ejecutar la próxima instrucción siguiente al fork(). Como los dos
procesos ejecutan el mismo programa y ambos ejecutan el print, el output corresponde al del padre
y al del hijo dando como resultado dos Hola mundo!."
    
}

l_funcion()
{
    echo " ";
    echo "Identificar la condicion de carrera entre los threads: 
    La condición de carrera en este ejercicio se produce cuando el Thread leer se ejecuta antes de del encargado de escribir
    y por lo tanto se produce un error al intentar leer una variable que no tiene nada escrita."
    echo " ";
    echo "Identificar la sección crtica:
    thread escritor{
	    while(true){
	    	/*escribir datos*/ // Esta es la sección critica.
	    }
    }   

    thread lector
	    while(true){
		    /*leer datos*/ // Esta es la sección critica.
	    }
    }"

echo " ";

echo "Es necesario agregar semáforos de sincronización? 
ue necesaria la implementación de semáforos para impedir que el proceso leer 
se ejecute antes del proceso escribir, y este último método habilita al proceso leer";

echo " ";

echo "Pseudocodigo: 
escribir()
{
	sem_w(&escribir);
	mutex_lock();
	while(true)
	{
		/*escribir datos*/
	}
	sem_p(&leer);
	mutex_unlock();
}

leer()
{
	sem_w(&leer);
	mutex_lock();
	while(true)
	{
		/*leer datos*/
	}
	sem_p(&escribir);
	mutex_unlock();
}";
echo " ";
echo "Ejecutando codigo MUTEX: ";
 gcc -g -pthread ejercicioMutex.c -lpthread -o ejMutex
./ejMutex
}

m_funcion()
{
    echo " ";
    echo "Ejecutando codigo SINCRONIZACION: ";
    gcc -g -pthread ejercicioSincronizacion.c -lpthread -o ejSincronizacion
    ./ejSincronizacion
    echo "Ejecutando codigo SINCRONIZACIONBIEN: ";
    gcc -g -pthread ejercicioSincroBien.c -lpthread -o ejercicioSincroBien
    ./ejercicioSincroBien

}

n_funcion()
{
    echo " ";
    echo "Ejecutando paralelismo de datos secuencial y midiendo su tiempo de ejecucion: ";
    gcc -g -pthread pdatossecuencial.c -lpthread -o pdatossecuencial
    time ./pdatossecuencial
    echo " ";
    echo "Ejecutando paralelismo de datos con threads y midiendo su tiempo de ejecucion: ";
    gcc -g -pthread paralelismo.c -lpthread -o paralelismo
    time ./paralelismo
    echo " ";
    echo "Ejecutando paralelismo de tareas secuencial y midiendo su tiempo de ejecucion: ";
    gcc -g -pthread ptareassecuencial.c -lpthread -o ptareassecuencial
    time ./ptareassecuencial
    echo " ";
    echo "Ejecutando paralelismo de tareas con threads y midiendo su tiempo de ejecucion: ";
    gcc -g -pthread ptareas.c -lpthread -o ptareas
    time ./ptareas
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 2. leer la opcion del usuario
    # 1. mostrar el menu
    imprimir_menu;
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
	    h|H) h_funcion;;
        i|I) i_funcion;;
        j|J) j_funcion;;
        k|K) k_funcion;;
        l|L) l_funcion;;
        m|M) m_funcion;;
        n|N) n_funcion;;
        

        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done