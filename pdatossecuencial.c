#include "stdio.h"
#include "stdlib.h"

int main(void)
{
    int sumaTotal = 0 ;
    // creacion del arreglo pi y lectura del archivo
    int digitosPi[10000];
    FILE *archivo;
    archivo = fopen("10milDigitosDePi_separados.txt", "r");

    if (archivo == NULL)
    {
        printf("Error en la lectura\n");
        exit(0);
    }

    for (int i = 0; i < 10000; i++)
    {
        fscanf(archivo, "%i,", &digitosPi[i]);
    }

    fclose(archivo);

    for (int i = 0; i < 10000; i++)
    {
        sumaTotal = sumaTotal + digitosPi[i];
    }

    printf("La suma total de los primeros digitos de Pi es: %i \n", sumaTotal);

    return 0;
}